package parrot;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ParrotTestNorwegianBlue {

    @Test
    public void getSpeedNorwegianBlueParrot_nailed() {
        ParrotNorwegianBlue parrot = new ParrotNorwegianBlue(0, 1.5, true);
        assertEquals(0.0, parrot.getSpeed(), 0.0);
    }

    @Test
    public void getSpeedNorwegianBlueParrot_not_nailed() {
        ParrotNorwegianBlue parrot = new ParrotNorwegianBlue(0, 1.5, false);
        assertEquals(18.0, parrot.getSpeed(), 0.0);
    }

    @Test
    public void getSpeedNorwegianBlueParrot_not_nailed_high_voltage() {
        ParrotNorwegianBlue parrot = new ParrotNorwegianBlue(0, 4, false);
        assertEquals(24.0, parrot.getSpeed(), 0.0);
    }
}
