package parrot;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ParrotAfricanTest {

    @Test
    public void getSpeedOfAfricanParrot_With_One_Coconut2() {
        Parrot parrot = new ParrotAfrican( 1, 0, false);
        assertEquals(3.0, parrot.getSpeed(), 0.0);
    }

    @Test
    public void getSpeedOfAfricanParrot_With_Two_Coconuts2() {
        Parrot parrot = new ParrotAfrican(2, 0, false);
        assertEquals(0.0, parrot.getSpeed(), 0.0);
    }

    @Test
    public void getSpeedOfAfricanParrot_With_No_Coconuts2() {
        Parrot parrot = new ParrotAfrican(0, 0, false);
        assertEquals(12.0, parrot.getSpeed(), 0.0);
    }
}
