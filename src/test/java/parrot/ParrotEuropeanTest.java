package parrot;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ParrotEuropeanTest {

    @Test
    public void getSpeedOfEuropeanParrot2() {
        Parrot parrot = new ParrotEuropean(0, 0, false);
        assertEquals(12.0, parrot.getSpeed(), 0.0);
    }
}
